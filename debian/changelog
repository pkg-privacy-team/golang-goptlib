golang-goptlib (1.6.0-3) unstable; urgency=medium

  * Update watch file.

 -- Danial Behzadi <dani.behzi@ubuntu.com>  Mon, 06 Jan 2025 13:29:08 +0330

golang-goptlib (1.6.0-2) unstable; urgency=medium

  [ Simon Josefsson ]
  * Team upload.
  * Bump Standards-Version to 4.7.0.
  * Add symbolic link for old namespace, for snowflake FTBFS. Closes: #1090302.
  * Add myself to debian/copyright.

  [ Helmut Grohne ]
  * Mark golang-goptlib-dev Multi-Arch: foreign. Closes: #984699.

 -- Simon Josefsson <simon@josefsson.org>  Sat, 21 Dec 2024 10:10:31 +0100

golang-goptlib (1.6.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 06 Dec 2024 13:36:18 -0500

golang-goptlib (1.5.0-1) unstable; urgency=medium

  * Update source in d/watch.
  * Use '_build' as build root.
  * New upstream version 1.5.0

 -- Danial Behzadi <dani.behzi@ubuntu.com>  Thu, 22 Feb 2024 01:56:29 +0330

golang-goptlib (1.2.0-1) unstable; urgency=medium

  [ Antoine Beaupré ]
  * Team upload
  * fix tests with newer release
  * fix Vcs-* URLs
  * remove lintian override (`no-upstream-changelog`) that was
    triggering... a lintian error!

  [ Ruben Pollan ]
  * New upstream release.
  * Update to debhelper 13, newer policy, no change.
  * Update to latest Standards-Version.

 -- Antoine Beaupré <anarcat@debian.org>  Thu, 26 May 2022 14:27:13 -0400

golang-goptlib (0.6-3) unstable; urgency=medium

  * Team upload.
  * Switch to XS-Go-Import-Path

 -- Michael Stapelberg <stapelberg@debian.org>  Fri, 09 Feb 2018 16:49:10 +0100

golang-goptlib (0.6-2) unstable; urgency=medium

  * Team upload.

  * Declare compliance with policy version 4.1.3
    * debian/copyright: Use HTTPS URI for the file format
    * Update debian/copyright to refer to the text of the CC0 license in
      /usr/share/common-licenses
  * Fix debian/watch

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Thu, 11 Jan 2018 15:01:46 +0100

golang-goptlib (0.6-1) unstable; urgency=medium

  * New upstream release.
  * Update to latest Standards-Version. No changes required.

 -- Ximin Luo <infinity0@debian.org>  Thu, 12 May 2016 17:12:42 +0200

golang-goptlib (0.5-1) unstable; urgency=medium

  * New upstream release.

 -- Ximin Luo <infinity0@pwned.gg>  Mon, 06 Jul 2015 12:04:53 +0200

golang-goptlib (0.4-1) unstable; urgency=medium

  * New upstream release.
  * Update to latest Standards-Version.

 -- Ximin Luo <infinity0@pwned.gg>  Fri, 17 Apr 2015 10:44:34 +0200

golang-goptlib (0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Ximin Luo <infinity0@pwned.gg>  Sun, 17 Aug 2014 18:27:11 +0100

golang-goptlib (0.1-1) unstable; urgency=low

  * Initial release (Closes: #739599)

 -- Ximin Luo <infinity0@pwned.gg>  Sun, 23 Feb 2014 20:53:56 +0000
